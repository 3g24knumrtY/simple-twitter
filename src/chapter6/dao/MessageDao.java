package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
      		sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // user_id
            sql.append("    ?, ");                                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public UserMessage edit(Connection connection, Integer id, int LIMIT_NUM) {

		PreparedStatement ps = null;
	    try {
	    	String sql = "SELECT * from messages where id = ?";

	    	ps = connection.prepareStatement(sql);

	    	ps.setInt(1, id);

	    	ResultSet rs = ps.executeQuery();

	    	List<UserMessage> messages = toUserMessages(rs);
	    	if (messages.isEmpty()) {
	    		return null;
	    	} else {
	    		return messages.get(0);
	    	}

	    	} catch (SQLException e) {
	    		throw new SQLRuntimeException(e);
	    	} finally {
	    		close(ps);
	    	}
	  	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, Message message) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
  			sql.append("UPDATE messages SET");
  			sql.append(" text = ?, ");
  			sql.append(" created_date = CURRENT_TIMESTAMP,");
  			sql.append(" updated_date = CURRENT_TIMESTAMP");
  			sql.append(" where id = ? ");

  			ps = connection.prepareStatement(sql.toString());

  	        ps.setString(1, message.getText());
  	        ps.setInt(2,message.getId());


  	        int count = ps.executeUpdate();
  	        if (count == 0) {
  	        	throw new NoRowsUpdatedRuntimeException();
  	        }
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }
	}













  	public void delete(Connection connection, Message deleteId) {
  		PreparedStatement ps = null;
  		try {
  			String sql = "DELETE FROM messages WHERE id = ?"; //データベースにアクセスするためのsql文を作成。

  			ps = connection.prepareStatement(sql);	//作成したsql文をDBに送りpsに格納。
  			ps.setInt(1, deleteId.getId());  //

  			int count = ps.executeUpdate();  //sql文を実行。
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }

  		} catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
  	}
}