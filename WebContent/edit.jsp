<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="chapter6.beans.Message" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>つぶやき編集画面</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">

    </head>
	<body>


		<c:if test="${ not empty errorMessages}">
			<div class="errorMessages">
				<ul>
        			<c:forEach items="${errorMessages}" var="errorMessage">
        				<li><c:out value="${errorMessage}" />
       				</c:forEach>
       			</ul>
       			<c:remove var="errorMessages" scope="session" />
       		</div>
       	</c:if>



		<form action="edit" method="post">
			<div class="messages">
					<div class="message">
						<div class="account-name">
						</div>
						<div class="text">
							つぶやき
							<div class="form-area">
           						<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${messages.text}" /></textarea>
           						<input type = "hidden" name = "id" value = "${messages.id}">
							</div>
						</div>
						<div class="date">
							<fmt:formatDate value="${messages.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						<input type="submit" value="更新">

					</div>
			</div>
		</form>
		<br>
		<br>
		<br>
		<a href="./">戻る</a>
		<br>
		<br>
		<div class="copyright"> Copyright(c)SatoKyohei</div>
	</body>
</html>