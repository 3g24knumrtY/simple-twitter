package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter({"/setting", "/edit"})

public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//セッションを取得する。
		HttpSession session = ((HttpServletRequest)request).getSession();
		//セッションから値(loginUser)を取得して変数loginUserへ格納。
		User loginUser = (User) session.getAttribute("loginUser");
		((HttpServletRequest) request).getServletPath();
		String servletPath = ((HttpServletRequest) request).getServletPath();

		if (servletPath.equals("/login.jsp") || (servletPath.equals ("/login")) || loginUser != null) {
			chain.doFilter(request, response); //サーブレットを実行する処理。これがないと白紙ページになる。

		} else {
			String errorMessage = "ログインしてください";
			session.setAttribute("errorMessages", errorMessage);
			((HttpServletResponse) response).sendRedirect("./login");
			return;
		}

	}

	public void init(FilterConfig filterConfig)throws ServletException {
	}

	public void destroy() {
	}


}

