<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="chapter6.beans.Message" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/jquery-3.6.0.js"></script>
        <script src="js/test.js"></script>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
            	<c:if test="${ empty loginUser }">
                	<a href="login">ログイン</a>
                	<a href="signup">登録する</a>
                 </c:if>
                 <%--loginUserがいれば表示。ホーム画面のブロック --%>
                 <c:if test="${ not empty loginUser }">
      			 	<a href="./">ホーム</a>
       			 	<a href="setting">設定</a>
      			  	<a href="logout">ログアウト</a>
  				 </c:if>
            </div>


			<div style="display:inline-flex">
            	<form action="./" method="get">
            		<label for="start">日付</label>
            		<input type="date" name="start" id=start value="${start}">～
            		<label for="end"></label>
            		<input type="date" name="end" id=end value="${end}" />
            		<input type="submit" value="絞込"></input>
            	</form>
            </div>


			<%--loginUserがいれば表示。ホーム画面の表示。 --%>
            <c:if test="${ not empty loginUser }">
            	<div class="profile">
       				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
        			<div class="account">@<c:out value="${loginUser.account}" /></div>
        			<div class="description"><c:out value="${loginUser.description}" /></div>
    			</div>
			</c:if>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<%--ホーム画面でつぶやきの枠を作っているブロック。messageサーブレットへpost送信 --%>
			<div class="form-area">
    			<c:if test="${ isShowMessageForm }">
        			<form action="message" method="post">
            			いま、どうしてる？<br />
            			<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
            			<br />
            			<input type="submit" value="つぶやく">（140文字まで）
        			</form>
    			</c:if>
			</div>

             <%-- アカウントとメッセージを表示させる処理、ここを変える --%>
			<div class="messages">
				<c:forEach items="${messages}" var="message">
					<div class="message">
						<div class="account-name">								<span class="account">
							<a href="./?user_id=<c:out value="${message.userId}"/> ">
							<c:out value="${message.account}" /></a></span>
							<span class="name"><c:out value="${message.name}" /></span>
						</div>
						<c:forEach var="edit" items="${message.splitedText}">
								 	<div>${edit}</div>
						</c:forEach>
						<div class="date">
							<fmt:formatDate value="${message.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

						<div style="display:inline-flex">
							<form action = "edit" method = "get">
								<c:if test="${loginUser.id == message.userId }">
									<input type="hidden" name="messageId"  value="${message.id}"/>  <%-- --%>
									<input type="submit"  value="編集" >
								</c:if>
								&nbsp;
							</form>

							<form action = "deleteMessage" method = "post">
								<c:if test="${loginUser.id == message.userId }">
									<input type="hidden" name="deleteId"  value="${message.id}"  />
									<input type="submit"   value="削除" id="delete" >
								</c:if>

							</form>
							<br>
						</div>

						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId}">
								<div class="account-name">
									<span class="account">
										<a href="./?user_id=<c:out value="${comment.userId}"/> ">
										<c:out value="${comment.account}" /></a>
									</span>
									<span class="name"><c:out value="${comment.name}" /></span>
								</div>
								<c:forEach var="reply" items="${comment.splitedText}">
								 	<div>${reply}</div>
								</c:forEach>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</c:if>
						</c:forEach>

						<br><br>

						<div>
							<c:if test="${ not empty loginUser }">
							<form action = "comment" method = "post">
							 	返信<br>								<%--replyボックスにクラス変更 --%>
							 	<textarea name="text" cols="100" rows="5" class="reply-box"></textarea>
							 	<input type="hidden" name="messageId"  value="${message.id}" />
							 	<input type="hidden" name="userId"  value="${loginUser.id}"/>
							 	 <input type="submit"  value="返信" ><br><br>

							</form>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>

			<br>
			<br>
			<div class="copyright"> Copyright(c)SatoKyohei</div>
        </div>
    </body>
</html>