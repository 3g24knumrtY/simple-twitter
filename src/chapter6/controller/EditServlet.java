package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

    	String messageId = request.getParameter("messageId");
    	if(!check(messageId, errorMessages)) {
    		session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
    	}

    	UserMessage messages = new MessageService().edit(messageId);
    	if(!messageId.matches("^[0-9]+$") || messages == null) {
    		errorMessages.add("不正なパラメーターが入力されました");
    		session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
    	}



    	//リクエストパラメータにmessagesを入れてedit.jspへ移動。
    	request.setAttribute("messages", messages);
    	request.getRequestDispatcher("/edit.jsp").forward(request, response);
    }

    private boolean check(String messageId, List<String> errorMessages) {

        if(StringUtils.isEmpty(messageId) || !messageId.matches("^[0-9]+$")) {
        	errorMessages.add("不正なパラメーターが入力されました");
        	return false;
        }
        return true;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");

        Message message = getMessage(request);
        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("messages", message);
            request.getRequestDispatcher("/edit.jsp").forward(request, response);
            return;

        }


        new MessageService().update(message);
        request.setAttribute("messages", message);


        response.sendRedirect("./");
    }


	private Message getMessage(HttpServletRequest request)
			throws IOException, ServletException {

		Message message = new Message();
		message.setText(request.getParameter("text"));
		message.setId(Integer.parseInt(request.getParameter("id")));
		return message;
	}

	 private boolean isValid(String text, List<String> errorMessages) {

		 if (StringUtils.isBlank(text)) {
			 errorMessages.add("メッセージを入力してください");
	     } else if (140 < text.length()) {
	         errorMessages.add("140文字以下で入力してください");
	     }
	     if (errorMessages.size() != 0) {
	    	 return false;
	     }

	     return true;
	 }
}
