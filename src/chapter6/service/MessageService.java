package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ここに飛ぶ。メッセージ一覧を取得するためのコード
    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
              id = Integer.parseInt(userId);
            }
            String startDate = null;
            String endDate = null;
            Calendar cal1 = Calendar.getInstance();
            Date date1 = cal1.getTime();
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String nowTime = sdformat.format(date1);

            if (!StringUtils.isEmpty(start)) {
            	startDate = start + " "+ "00:00:00";
            } else {
            	startDate = "2021-01-01 00:00:00";
            }

            if (!StringUtils.isEmpty(end)) {
            	endDate = end + " "+ "23:59:59";
            } else {
            	endDate = nowTime;
            }

            //リスト型messagesの中にUserMessageDaoクラスのselectメソッドを呼び出し代入。
            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public UserMessage edit(String messageId) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            if (!StringUtils.isEmpty(messageId)) {
              id = Integer.parseInt(messageId);
            }
            UserMessage messages = new MessageDao().edit(connection, id, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {

    	Connection connection = null;
    	try {
    		connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(Message deleteId) {

    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, deleteId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

}