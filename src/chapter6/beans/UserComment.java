package chapter6.beans;

import java.sql.Timestamp;
import java.util.Date;

public class UserComment {


    private int id;
    private String account;
    private String name;
    private int messageId;
	private int userId;
    private String text;
    private Date createdDate;
    private Date updatedDate;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date created_date) {
		this.createdDate = created_date;
	}
	public void setUpdatedDate(Timestamp timestamp) {

	}
	 public int getMessageId() {
			return messageId;
		}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String[] getSplitedText() {
		return text.split("\n");
	}

}
